<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Http\Model\User;

class loginController extends Controller
{
    public function index(){
        return view('admin.login');
    }

    public function login(){
        if($input = Input::all()){

            $username = $input['user_name'];
            $password = $input['user_pass'];
            $user = User::where('name',$username)->first();
            $passwordFromDB = $user->Password;
            if( Crypt::decrypt($passwordFromDB) ==  $password){
                session(['user'=>$username]);
                return redirect('admin/index');
            }else{
                return back()->with('msg','password is wrong');
            }
        }else{
            // session(["user"=>null]);
            return view('admin.login');
        }
    }

    public function crypt(){
        $str = '123123';
        echo Crypt::encrypt($str);
    }
}



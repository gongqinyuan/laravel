<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Model\category;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    public function index(){

        $category = category::orderBy('id','desc')->get();
        $data = [
            'value1'=>111,
            'value2'=>222,
            'category'=>$category
        ];
        return view('admin.index')->with('data',$data);

    }

    public function updateAge(){
        $input = Input::all();
        $id = $input['id'];
        $title = $input['title'];
        $category = category::find($id);
        $category->cate_title = $title;
        $res = $category->update();

        if($res){
            return 'success';
        }else{
            return 'fail';
        }
    }
}

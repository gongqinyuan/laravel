<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('../resources/views/css/style.css') }}">
    
    
</head>
<body>
    <div class='formblock'>
        <h1>Login Page</h1>
        <div>
            <form action="" method="post">
                {{ csrf_field() }}
                @if (session("msg"))
                    <p>{{session("msg")}}</p>
                @endif
                <div>Name:<input name='user_name' type="text"></div>
                <div>Password:<input name='user_pass' type="password"></div>
                <div><input type="submit" value="submit"></div>
            </form>
        </div>

    </div>

    
    
</body>
</html>
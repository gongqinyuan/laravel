@extends('layout.admin')
@section('content')

    <div style="margin-bottom:20px;">
        update title by ajax: <input type="text" onchange="updateTitle(this,1)" >
    </div>



    <form action="updateage" method="post">
        {{ csrf_field() }}
        Input Title: <input type="text" name='title'>
        Id: <input type="text" name='id'>
        <button type = 'submit'>Submit</button>
    </form>


    @foreach ($data as $key=>$item)
        <div>{{$key . '--' . $item}}</div>
    @endforeach
    <script>
        $(document).ready(function(){
            function updateTitle(){
                $.ajax({
                    url: "updateage", 
                    data:{
                        _token:{{ csrf_token() }},

                    },
                    success: function(result){
                    $("#div1").html(result);
                }});
            }
            
        })

    </script>
@endsection